<?php
$filepath = realpath(dirname(__FILE__));
include_once ($filepath.'/../lib/Database.php');
include_once ($filepath.'/../helpers/Format.php');
?>
<?php

class Subcat{
    private $db;
    private $fm;

    public function __construct(){
        $this->db = new Database();
        $this->fm = new Format();
    }
    public function brandInsert($brandName){
        $brandName = $this->fm->validation($brandName);
        $brandName = mysqli_real_escape_string($this->db->link,$brandName);
        if(empty($brandName)){
            $msg = "<span class='error'>Sub-Category name must not be empty!</span>";
            return $msg;
        }
        else{
            $query = "INSERT INTO `tbl_subcat` (`brandId`, `brandName`) VALUES (NULL, '$brandName');";
            $insertBrand = $this->db->insert($query);
            if($insertBrand){
                $msg = "<span class='success'>Sub-Category Inserted Successfully!</span>";
                return $msg;
            }else{
                $msg = "<span class='error'>Sub-Category has not been Inserted!</span>";
                return $msg;
            }
        }
    }
    public function getAllBrand(){
        $query = "SELECT * FROM `tbl_subcat` ORDER BY `brandId` DESC;";
        $result = $this->db->select($query);
        return $result;
    }
    public function getBrandById($id)
    {
        $query = "SELECT * FROM `tbl_subcat` WHERE `brandId` = '$id';";
        $result = $this->db->select($query);
        return $result;
    }
    public function brandUpdate($brandName, $id){
        $brandName = $this->fm->validation($brandName);
        $brandName = mysqli_real_escape_string($this->db->link,$brandName);
        $id = mysqli_real_escape_string($this->db->link,$id);
        if(empty($brandName)) {
            $msg = "<span class='error'>Sub-Category Name must not be empty!</span>";
            return $msg;
        } else{
            $query = "UPDATE `tbl_subcat` SET `brandName` = '$brandName' WHERE `brandId` ='$id';";
            $updated_row = $this->db->update($query);

            if($updated_row){
                $msg = "<span class='success'>Sub-Category Name Updated Successfully!</span>";
                return $msg;
            } else{
                $msg = "<span class='error'>Sub-Category Name has not been Updated!</span>";
                return $msg;
            }
        }
    }

    public function delBrandById($id){
        $query = "DELETE FROM `tbl_subcat` WHERE `brandId` = '$id';";
        $deldata = $this->db->delete($query);
        if($deldata){
            $msg = "<span class='success'>Sub-Category Name Deleted Successfully!</span>";
            return $msg;
        }else{
            $msg = "<span class='error'>Sub-Category Name has not been Deleted!</span>";
            return $msg;
        }
    }
}
?>

