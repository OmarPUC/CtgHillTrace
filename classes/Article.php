<?php
    $filepath = realpath(dirname(__FILE__));
    include_once ($filepath.'/../lib/Database.php');
    include_once ($filepath.'/../helpers/Format.php');
?>
<?php

class Article{
    private $db;
    private $fm;

    public function __construct(){
        $this->db = new Database();
        $this->fm = new Format();
    }
    public function productInsert($data,$file){
        $productName = mysqli_real_escape_string($this->db->link,$data['productName']);
        $catId       = mysqli_real_escape_string($this->db->link, $data['catId']);
        $brandId     = mysqli_real_escape_string($this->db->link, $data['brandId']);
        $body        = mysqli_real_escape_string($this->db->link, $data['body']);
        $type        = mysqli_real_escape_string($this->db->link, $data['type']);

        $permited  = array('jpg', 'jpeg', 'png', 'gif');
        $file_name = $file['image']['name'];
        $file_size = $file['image']['size'];
        $file_temp = $file['image']['tmp_name'];

        $div = explode('.', $file_name);
        $file_ext = strtolower(end($div));
        $unique_image = substr(md5(time()), 0, 10).'.'.$file_ext;
        $uploaded_image = "upload/".$unique_image;

        if($productName == "" || $catId == "" || $brandId == "" || $body == "" || $uploaded_image == "" || $type == ""){
            $msg = "<span class='error'>Fields must not be empty!</span>";
            return $msg;
        }
        elseif ($file_size >103575837588358) {
            echo "<span class='error'>Image Size should be less then 20MB!</span>";
        } elseif (in_array($file_ext, $permited) === false) {
            echo "<span class='error'>You can upload only:-"
                .implode(', ', $permited)."</span>";
        }
        else{
            move_uploaded_file($file_temp, $uploaded_image);
            $query = "INSERT INTO `tbl_article`(productName, catId, brandId, body, image, type) VALUES ('$productName','$catId','$brandId','$body','$uploaded_image','$type')";
            $insert_pro = $this->db->insert($query);
            if($insert_pro){
                $msg = "<span class='success'>Article Inserted Successfully!</span>";
                return $msg;
            }else{
                $msg = "<span class='error'>Article has not been Inserted!</span>";
                return $msg;
            }
        }
    }
    public function getAllProduct(){
     $query = "SELECT p.*, c.catName, b.brandName
                FROM tbl_article as p, tbl_category as c, tbl_subcat as b 
                WHERE p.catId = c.catId AND p.brandId = b.brandId
                ORDER BY p.productId DESC";

        $result = $this->db->select($query);
        return $result;
    }
    public function getProById($id){
        $query = "SELECT * FROM `tbl_article` WHERE `productId` = '$id';";
        $result = $this->db->select($query);
        return $result;
    }
    public function productUpdate($data,$file,$id){
        $productName = mysqli_real_escape_string($this->db->link,$data['productName']);
        $catId       = mysqli_real_escape_string($this->db->link, $data['catId']);
        $brandId     = mysqli_real_escape_string($this->db->link, $data['brandId']);
        $body        = mysqli_real_escape_string($this->db->link, $data['body']);
        $type        = mysqli_real_escape_string($this->db->link, $data['type']);

        $permited  = array('jpg', 'jpeg', 'png', 'gif');
        $file_name = $file['image']['name'];
        $file_size = $file['image']['size'];
        $file_temp = $file['image']['tmp_name'];

        $div = explode('.', $file_name);
        $file_ext = strtolower(end($div));
        $unique_image = substr(md5(time()), 0, 10).'.'.$file_ext;
        $uploaded_image = "upload/".$unique_image;

        if($productName == "" || $catId == "" || $brandId == "" || $body == "" || $uploaded_image == "" || $type == ""){
            $msg = "<span class='error'>Fields must not be empty!</span>";
            return $msg;
        }
        else {
            if(!empty($file_name)) {

                if ($file_size > 20971520) {
                    echo "<span class='error'>Image Size should be less then 20MB!</span>";
                } elseif (in_array($file_ext, $permited) === false) {
                    echo "<span class='error'>You can upload only:-"
                        . implode(', ', $permited) . "</span>";
                } else {
                    move_uploaded_file($file_temp, $uploaded_image);

                    $query = "UPDATE tbl_article 
                    SET
                    productName = '$productName',
                    catId       = '$catId',
                    brandId     = '$brandId',
                    body        = '$body',
                    image       = '$uploaded_image',
                    type        = '$type'
                    WHERE productId = '$id'
                    ";
                    $updated_pro = $this->db->update($query);
                    if ($updated_pro) {
                        $msg = "<span class='success'>Article updated Successfully!</span>";
                        return $msg;
                    } else {
                        $msg = "<span class='error'>Article has not been updated!</span>";
                        return $msg;
                    }
                }
            }
            else{
                $query = "UPDATE tbl_article 
                    SET
                    productName = '$productName',
                    catId       = '$catId',
                    brandId     = '$brandId',
                    body        = '$body',
                    type        = '$type'
                    WHERE productId = '$id'
                    ";
                $updated_pro = $this->db->update($query);
                if ($updated_pro) {
                    $msg = "<span class='success'>Article updated Successfully!</span>";
                    return $msg;
                } else {
                    $msg = "<span class='error'>Article has not been updated!</span>";
                    return $msg;
                }
            }
        }
    }
    public function delProById($id){
        $query = "SELECT * FROM tbl_article WHERE productId = '$id'";
        $getData = $this->db->select($query);
        if($getData){
            while ($delImg = $getData->fetch_assoc()){
                $dellink = $delImg['image'];
                unlink($dellink);
            }
        }
        $delquery = "DELETE FROM tbl_article WHERE productId = '$id'";
        $deldata = $this->db->delete($delquery);
        if($deldata){
            $msg = "<span class='success'>Article Deleted Successfully!</span>";
            return $msg;
        }else{
            $msg = "<span class='error'>Article has not been Deleted!</span>";
            return $msg;
        }
    }
    public function getFeaturedProduct(){
        $query = "SELECT * FROM `tbl_article` WHERE type='0' ORDER BY `productId` DESC LIMIT 4;";
        $result = $this->db->select($query);
        return $result;
    }

    public function getNewProduct(){
        $query = "SELECT * FROM `tbl_article` ORDER BY `productId` DESC LIMIT 4;";
        $result = $this->db->select($query);
        return $result;
    }
    public function getSingleProduct($id){
        $query = "SELECT p.*, c.catName, b.brandName
                FROM tbl_article as p, tbl_category as c, tbl_subcat as b 
                WHERE p.catId = c.catId AND p.brandId = b.brandId AND p.productId = '$id'";
        $result = $this->db->select($query);
        return $result;
    }
    public function latestFromIphone(){
        $query = "SELECT * FROM `tbl_article` WHERE `brandId` = '2' ORDER BY `productId` DESC LIMIT 1;";
        $result = $this->db->select($query);
        return $result;
    }

    public function latestFromSamsung(){
        $query = "SELECT * FROM `tbl_article` WHERE `brandId` = '3' ORDER BY `productId` DESC LIMIT 1;";
        $result = $this->db->select($query);
        return $result;
    }

    public function latestFromAcer(){
        $query = "SELECT * FROM `tbl_article` WHERE `brandId` = '4' ORDER BY `productId` DESC LIMIT 1;";
        $result = $this->db->select($query);
        return $result;
    }

    public function latestFromCanon(){
        $query = "SELECT * FROM `tbl_article` WHERE `brandId` = '1' ORDER BY `productId` DESC LIMIT 1;";
        $result = $this->db->select($query);
        return $result;
    }
    public function productByCat($id){
        $catId  = mysqli_real_escape_string($this->db->link, $id);
        $query  = "SELECT * FROM `tbl_article` WHERE `catId` = '$catId';";
        $result = $this->db->select($query);
        return $result;
    }
}
?>