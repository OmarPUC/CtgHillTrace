<?php
    include 'lib/Session.php';
    Session::init();
    include 'helpers/Format.php';
    include 'lib/Database.php';

spl_autoload_register(function($class){
    include_once "classes/".$class.".php";
});
    $db = new Database();
    $fm = new Format();
    $pd = new Article();
    $cat = new Category();
?>



<?php
header("Cache-Control: no-cache, must-revalidate");
header("Pragma: no-cache");
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");
header("Cache-Control: max-age=2592000");
?>
<!DOCTYPE HTML>
<head>
    <title>CHT</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link href="css/style.css" rel="stylesheet" type="text/css" media="all"/>
    <link href="css/menu.css" rel="stylesheet" type="text/css" media="all"/>
    <script src="js/jquerymain.js"></script>
    <script src="js/script.js" type="text/javascript"></script>
    <script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="js/jquery-1.12.4.min.js"></script>
    <script type="text/javascript" src="js/jquery-1.7.2.min.js"></script>
    <script type="text/javascript" src="js/nav.js"></script>
    <script type="text/javascript" src="js/move-top.js"></script>
    <script type="text/javascript" src="js/easing.js"></script>
    <script type="text/javascript" src="js/nav-hover.js"></script>
    <link href='http://fonts.googleapis.com/css?family=Monda' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Doppio+One' rel='stylesheet' type='text/css'>
    <script type="text/javascript">
        $(document).ready(function($){
            $('#dc_mega-menu-orange').dcMegaMenu({rowItems:'4',speed:'fast',effect:'fade'});
        });
    </script>




</head>
<body>
<div class="wrap">
    <div class="header_top">
        <div class="logo">
            <a href="full-page-slider/home.php?_ijt=o4vksi176vmfs4vc8997d6qjfi"><img src="images/bhtc3.png" alt="" /></a>
        </div>
        <div class="header_top_right">

            <div class="search_box">

                <script>
                    (function() {
                        var cx = '004366608668507735345:9ghyje02u10';
                        var gcse = document.createElement('script');
                        gcse.type = 'text/javascript';
                        gcse.async = true;
                        gcse.src = 'https://cse.google.com/cse.js?cx=' + cx;
                        var s = document.getElementsByTagName('script')[0];
                        s.parentNode.insertBefore(gcse, s);
                    })();
                </script>
                <gcse:search></gcse:search>
<!--                <div id="search_items"></div>-->
            </div>

            


            <!--   Jvascript file for search filtering START  -->


            <!--   Jvascript file for search filtering END  -->


            <!--login code will be there-->
            <div class="clear"></div>
        </div>
        <div class="clear"></div>
    </div>
    <div class="menu">
        <ul id="dc_mega-menu-orange" class="dc_mm-orange">
            <li><a href="index.php">Home</a></li>
            <li><a href="productbycat.php?catId=19">Tourism</a></li>
            <li><a href="productbycat.php?catId=18">Geography</a></li>
            <li><a href="productbycat.php?catId=17">Tribes</a></li>
            <li><a href="productbycat.php?catId=16">Culture</a> </li>
            <li><a href="">Gallery</a> </li>
            <li><a href="">Contact</a> </li>
            <li><a href="">About  CHT</a> </li>
            <div class="clear"></div>
        </ul>
    </div>