<div class="header_bottom">
    <div class="header_bottom_left">
        <div class="section group">
                        
            <?php
                $getIphone = $pd->latestFromIphone();
                if($getIphone){
                    while ($result = $getIphone->fetch_assoc()){
            ?>
            <div class="listview_1_of_2 images_1_of_2">
                <div class="listimg listimg_2_of_1">
                    <a href="details.php?proid=<?php echo $result['productId'];?>"> <img src="admin/<?php echo $result['image'];?>" alt="image" class="image"/></a>
                    <div class="middle">
                        <div class="textdetails">Details</div>
                    </div>
                </div>
            </div>
            <?php } } ?>


            <?php
            $getSamsung = $pd->latestFromSamsung();
            if($getSamsung){
            while ($result = $getSamsung->fetch_assoc()){
            ?>
            <div class="listview_1_of_2 images_1_of_2">
                <div class="listimg listimg_2_of_1">
                    <a href="details.php?proid=<?php echo $result['productId'];?>"> <img src="admin/<?php echo $result['image'];?>" alt="image" class="image" /></a>
                    <div class="middle">
                        <div class="textdetails">Details</div>
                    </div>
                </div>
            </div>
            <?php } } ?>
        </div>
        <div class="section group">



            <?php
            $getAcer = $pd->latestFromAcer();
            if($getAcer){
            while ($result = $getAcer->fetch_assoc()){
            ?>
            <div class="listview_1_of_2 images_1_of_2">
                <div class="listimg listimg_2_of_1">
                    <a href="details.php?proid=<?php echo $result['productId'];?>"> <img src="admin/<?php echo $result['image'];?>" alt="image" class="image" /></a>
                    <div class="middle">
                        <div class="textdetails">Details</div>
                    </div>
                </div>
            </div>
            <?php } } ?>



            <?php
            $getCanon = $pd->latestFromCanon();
            if($getCanon){
            while ($result = $getCanon->fetch_assoc()){
            ?>
            <div class="listview_1_of_2 images_1_of_2">
                <div class="listimg listimg_2_of_1">
                    <a href="details.php?proid=<?php echo $result['productId'];?>"> <img src="admin/<?php echo $result['image'];?>" alt="image" class="image" /></a>
                    <div class="middle">
                        <div class="textdetails">Details</div>
                    </div>
                </div>
            </div>
            <?php } } ?>

        </div>
        <div class="clear"></div>
    </div>
    <div class="header_bottom_right_images">
        <!-- FlexSlider -->

        <section class="slider">
            <div class="flexslider">
                <ul class="slides">
                    <li><img src="images/slide1.jpg" alt=""/></li>
                    <li><img src="images/slide2.jpg" alt=""/></li>
                    <li><img src="images/slide3.jpg" alt=""/></li>
                    <li><img src="images/slide4.jpg" alt=""/></li>
                    <li><img src="images/slide5.jpg" alt=""/></li>
                    <li><img src="images/slide6.jpg" alt=""/></li>
                    <li><img src="images/slide7.jpg" alt=""/></li>
                    <li><img src="images/slide8.jpg" alt=""/></li>
                </ul>
            </div>
        </section>
        <!-- FlexSlider -->
    </div>
    <div class="clear"></div>
</div>