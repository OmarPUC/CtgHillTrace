<?php include 'inc/header.php';?>
<?php include 'inc/sidebar.php';?>
<?php include '../classes/Subcat.php'; ?>
<?php include '../classes/Article.php'; ?>
<?php include '../classes/Category.php';?>

<?php

if (!isset($_GET['proid']) || $_GET['proid'] == NULL) {
    echo "<script>window.location = 'articlelist.php';</script>";
} else {
    $id = preg_replace('/[^-a-zA-Z0-9_]/', '',$_GET['proid']);
}

$pd = new Article();
if($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['submit'])){
    $updateProduct = $pd->productUpdate($_POST,$_FILES,$id);
}
?>

<div class="grid_10">
    <div class="box round first grid">
        <h2>Update Product</h2>
        <div class="block">
            <?php
            if(isset($updateProduct)){
                echo $updateProduct;
            }
            ?>
            <?php
                $getPord = $pd->getProById($id);
                if($getPord){
                    while ($value = $getPord->fetch_assoc()){
            ?>
            <form action="" method="post" enctype="multipart/form-data">
                <table class="form">

                    <tr>
                        <td>
                            <label>Name</label>
                        </td>
                        <td>
                            <input type="text" name="productName" value="<?php echo $value['productName'] ?>" placeholder="Enter Product Name..." class="medium" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Category</label>
                        </td>
                        <td>
                            <select id="select" name="catId">
                                <option>Select Category</option>
                                <?php
                                $cat = new Category();
                                $getCat = $cat->getAllCat();
                                if ($getCat){
                                    while ($result = $getCat->fetch_assoc()){
                                        ?>
                                        <option
                                            <?php
                                                if($value['catId'] == $result['catId']){   ?>
                                                    selected = "Selected";
                                             <?php  } ?>
                                                value="<?php echo $result['catId']; ?>"><?php echo $result['catName'];?>
                                        </option>
                                    <?php } } ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Sub-Category</label>
                        </td>
                        <td>
                            <select id="select" name="brandId">
                                <option>Select Sub-Category</option>
                                <?php
                                $brand = new Subcat();
                                $getBrand = $brand->getAllBrand();
                                if ($getBrand){
                                    while ($result = $getBrand->fetch_assoc()){
                                        ?>
                                        <option
                                            <?php
                                            if($value['brandId'] == $result['brandId']){   ?>
                                                selected = "selected";
                                            <?php  } ?>
                                                value="<?php echo $result['brandId']; ?>"><?php echo $result['brandName'];?>
                                        </option>
                                    <?php } } ?>
                            </select>
                        </td>
                    </tr>

                    <tr>
                        <td style="vertical-align: top; padding-top: 9px;">
                            <label>Description</label>
                        </td>
                        <td>
                            <textarea class="tinymce" name="body">
                                <?php echo $value['body']; ?>
                            </textarea>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <label>Upload Image</label>
                        </td>
                        <td>
                            <img src=" <?php echo $value['image'];?>" height="150px" width="200px"/><br/>
                            <input type="file" name="image" />
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <label>Article Type</label>
                        </td>
                        <td>
                            <select id="select" name="type">
                                <option>Select Type</option>
                                <?php
                                    if($value['type']==0){ ?>
                                        <option selected = "selected"; value="0">Featured</option>
                                        <option value="1">General</option>
                                 <?php   }  else { ?>
                                <option selected = "selected"; value="1">General</option>
                                        <option value="0">Featured</option>
                                        <?php } ?>
                            </select>
                        </td>
                    </tr>

                    <tr>
                        <td></td>
                        <td>
                            <input type="submit" name="submit" Value="Update"/>
                        </td>
                    </tr>
                </table>
            </form>
            <?php } } ?>
        </div>
    </div>
</div>
<!-- Load TinyMCE -->
<script src="js/tiny-mce/jquery.tinymce.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function () {
        setupTinyMCE();
        setDatePicker('date-picker');
        $('input[type="checkbox"]').fancybutton();
        $('input[type="radio"]').fancybutton();
    });
</script>
<!-- Load TinyMCE -->
<?php include 'inc/footer.php';?>