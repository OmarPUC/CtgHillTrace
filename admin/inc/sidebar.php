<div class="grid_2">
    <div class="box sidemenu">
        <div class="block" id="section-menu">
            <ul class="section menu">

                <li><a class="menuitem">Category</a>
                    <ul class="submenu">
                        <li><a href="catadd.php">Add Category</a> </li>
                        <li><a href="catlist.php">Category List</a> </li>
                    </ul>
                </li>
                <li><a class="menuitem">Sub-Category</a>
                    <ul class="submenu">
                        <li><a href="subcatadd.php">Add Sub-Category</a> </li>
                        <li><a href="subcatlist.php">Sub-Category List</a> </li>
                    </ul>
                </li>
                <li><a class="menuitem">Article</a>
                    <ul class="submenu">
                        <li><a href="articleadd.php">Add New Article</a> </li>
                        <li><a href="articlelist.php">Article Data List</a> </li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</div>