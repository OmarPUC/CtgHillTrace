<?php include 'inc/header.php';?>
<?php
if (!isset($_GET['proid']) || $_GET['proid'] == NULL) {
    echo "<script>window.location = '404.php';</script>";
} else {
    $id = preg_replace('/[^-a-zA-Z0-9_]/', '',$_GET['proid']);
}
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $quantity = $_POST['quantity'];
}
?>
    <div class="main">
        <div class="content">
            <div class="section group">
                <div class="cont-desc span_1_of_2">
                    <?php
                    $getPd =$pd->getSingleProduct($id);
                    if ($getPd){
                        while ($result = $getPd->fetch_assoc()){
                            ?>
                            <div class="grid images_3_of_2">
                                <img src="admin/<?php echo $result['image'];?>" alt="" />
                            </div>
                            <div class="desc span_3_of_2">
                                <h2><?php echo $result['productName'];?></h2>
                            </div>
                            <div class="product-desc">
                                <h2>Artitcle Details</h2>
                                <?php echo $result['body'];?>
                            </div>
                        <?php } } ?>

                    <div id="disqus_thread"></div>
                    <div id="disqus_thread"></div>

                    <div id="fb-root"></div>


                    <script>

                        /**
                         *  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
                         *  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables*/
                        /*
                       ID:   teamspartanb59@gmail.com
                       PASS: TEAM@spartanb59

                         var disqus_config = function () {
                         this.page.url = PAGE_URL;  // Replace PAGE_URL with your page's canonical URL variable
                         this.page.identifier = PAGE_IDENTIFIER; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
                         };
                         */
                        (function() { // DON'T EDIT BELOW THIS LINE
                            var d = document, s = d.createElement('script');
                            s.src = 'https://chtbd.disqus.com/embed.js';
                            s.setAttribute('data-timestamp', +new Date());
                            (d.head || d.body).appendChild(s);
                        })();
                    </script>
                    <noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>



                </div>

                <div class="rightsidebar span_3_of_1">
                    <h2>CATEGORIES</h2>
                    <ul>
                        <?php
                        $getCat = $cat->getAllCat();
                        if($getCat){
                            while ($result = $getCat->fetch_assoc()){
                                ?>
                                <li><a href="productbycat.php?catId=<?php echo $result['catId']?>"><?php echo $result['catName']?></a></li>
                            <?php }} ?>
                    </ul>

                </div>
            </div>
        </div>
    </div>
<?php include 'inc/footer.php';?>